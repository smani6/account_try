from django.shortcuts import render
from django.views.generic import View
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse
from rest_framework import viewsets
from rest_framework import serializers
from django.views.generic import (
    ListView, DetailView,
    UpdateView, CreateView, DeleteView)

from .models import Company, Client, Project,ProjectTimeSpentDetails

# Create your views here.

class Home(View):

    def get(self,request):
        return render (request,'home.html')

class Dashboard(View):
    """
        Dashboard View to display summary of number of clients,
        projects, company details and total cost incurred
    """
    def get(self, request):
        no_of_clients =  Client.objects.all().count()
        no_of_projects = Project.objects.all().count()
        no_of_companys = Company.objects.all().count()
        total_cost = 0
        projects = Project.objects.all()
        for project in projects:
            total_cost += project.get_total_cost()
        context_dict = {'no_of_clients' : no_of_clients,'no_of_projects': no_of_projects,'no_of_companys':no_of_companys,'total_cost':total_cost}
        return render(request, 'dashboard.html',{'context_dict':context_dict})

class ClientListView(ListView):
    """
        Client List View to display all the clients
    """
    model = Client
    template_name = 'clients/client_list.html'

class ClientCreateView(CreateView):
    """
        Client Create View to create new client
    """
    model = Client
    template_name = 'clients/client_form.html'
    fields = '__all__'
    success_url = reverse_lazy('client_list')

class ClientDetailView(DetailView):
    """
        Client Detail View to display detailed info about client
    """
    model = Client
    template_name = 'clients/client_detail.html'


class ClientUpdateView(UpdateView):
    """
        Client Update View to update client details
    """
    model = Client
    fields = ['name', 'email', 'company']
    template_name = 'clients/client_form.html'

class ClientDeleteView(DeleteView):
    """
        Client Delete View to delete client details
    """
    model = Client
    template_name = 'clients/client_delete.html'


class CompanyListView(ListView):
    """
        Company List View to display all the company's
    """
    model = Company
    template_name = 'companies/company_list.html'

class CompanyCreateView(CreateView):
    """
        Company Create View to create new company
    """
    model = Company
    template_name = 'companies/company_form.html'
    fields = '__all__'
    success_url = reverse_lazy('company_list')

class CompanyDetailView(DetailView):
    """
        Company Detail View to display detailed info about company
    """
    model = Company
    template_name = 'companies/company_detail.html'

class CompanyUpdateView(UpdateView):
    """
        Company Update View to update company details
    """
    model = Company
    fields = ['name', 'description']
    template_name = 'companies/company_form.html'

class CompanyDeleteView(DeleteView):
    """
        Company Delete View to delete company details
    """
    model = Company
    template_name = 'companies/company_delete.html'
    success_url = reverse_lazy('company_list')


class ProjectListView(ListView):
    """
        Project List View to display all the projects
    """
    model = Company
    template_name = 'companies/company_confirm_delete.html'
    success_url = reverse_lazy('company_list')




class ProjectListView(ListView):
    model = Project
    template_name = 'projects/project_list.html'


class ProjectCreateView(CreateView):

    """
        Project Create View to create new project
    """
    model = Project
    template_name = 'projects/project_form.html'
    fields = '__all__'
    success_url = reverse_lazy('project_list')


class ProjectDetailView(DetailView):

    """
        Project Detail View to display detailed info about project
    """
    model = Project
    template_name = 'projects/project_detail.html'


class ProjectUpdateView(UpdateView):
    """
        Project Update View to update project details
    """

    model = Project
    fields = ['name', 'client', 'start_date',
              'end_date', 'cost_per_hour']
    template_name = 'projects/project_form.html'


class ProjectDeleteView(DeleteView):
    """
        Project Delete View to delete project details
    """
    model = Project
    template_name = 'projects/project_delete.html'
    success_url = reverse_lazy('project_list')


class ProjectTimeListView(ListView):
    """
        Project Time List View to display all the projects timings
    """
    model = ProjectTimeSpentDetails
    template_name = 'project_time_spent/project_time_spent_list.html'


class ProjectTimeCreateView(CreateView):
    """
        Project Time Create View to create new project timings
    """
    model = ProjectTimeSpentDetails
    template_name = 'project_time_spent/project_time_spent_form.html'
    fields = ['project', 'started_at', 'ended_at']
    success_url = reverse_lazy('project_time_spent_list')


class ProjectTimeDetailView(DetailView):
    """
        Project Time Detail View to display detailed info about project timings
    """
    model = ProjectTimeSpentDetails
    context_object_name = "project_time_spent"
    template_name = \
        'project_time_spent/project_time_spent_detail.html'


class ProjectTimeUpdateView(UpdateView):
    """
        Project Time Update View to update project timings
    """
    model = ProjectTimeSpentDetails
    template_name = 'project_time_spent/project_time_spent_form.html'
    fields = ['project', 'started_at', 'ended_at']


class ProjectTimeDeleteView(DeleteView):
    """
        Project Time Delete View to delete project timing entries
    """
    model = ProjectTimeSpentDetails
    template_name = \
        'project_time_spent/project_time_spent_delete.html'
    success_url = reverse_lazy('project_time_spent_list')

class ReportsView(View):

    def __init__(self):
        pass

    def get(self,request):
        """
            Get method to return client and project details
            to be show in report page
        :param request:
        :return: context_dict - with client and projects details as reports
        """
        client_list = []
        client_details = Client.objects.all()
        for client in client_details:
            context_dict = {}
            context_dict['name'] = client.name
            context_dict['email'] = client.email
            context_dict['company_name'] = client.company.name
            context_dict['project_details'] = []
            total_hours = 0
            total_cost = 0
            no_of_projects = 0
            for project in client.project_set.all():
                hours = project.get_total_hours()
                cost = project.get_total_cost()
                total_hours += hours
                total_cost += cost
                context_dict['project_details'].append({project.id:project.name})
                no_of_projects +=1
            context_dict['total_hours'] = total_hours
            context_dict['total_cost'] = total_cost
            context_dict['no_of_projects'] = no_of_projects
            client_list.append(context_dict)

        project_list = []
        project_details = Project.objects.all()
        for project in project_details:
            project_context_dict = {}
            project_context_dict['name'] = project.name
            project_context_dict['client'] = project.client.name
            project_context_dict['start_date'] = project.start_date
            project_context_dict['end_date'] = project.end_date
            project_context_dict['cost_per_hour'] = project.cost_per_hour
            project_context_dict['total_hours'] = project.get_total_hours()
            project_context_dict['total_cost'] = project.get_total_cost
            project_list.append(project_context_dict)

        return render(request,'reports/reports_clients.html', {'context_dict':{'clients':client_list, 'projects':project_list }})

    def post(self,request):
        pass

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ('name', 'email',)

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Client.objects.all()
    serializer_class = UserSerializer