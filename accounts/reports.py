from accounts.models import Company,Client
try:
    from django.db.models.related import RelatedObject as ForeignObjectRel
except:
    # django 1.8 +
    from django.db.models.fields.related import ForeignObjectRel

from model_report.report import reports, ReportAdmin

class AnyModelReport(ReportAdmin):
    model = Company
    fields = [
        'name','description'
    ]

    type = 'report'

reports.register('anymodel-report', AnyModelReport)


class ClientReport(ReportAdmin):
    model = Client
    fields = [
        'name','email','company'
    ]

    type = 'report'

reports.register('client-report', ClientReport)