"""accounting URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url,patterns

from django.contrib import admin
from .views import (Home,Dashboard, ClientListView,ClientCreateView,
                    ClientDetailView,
                    ClientUpdateView, ClientDeleteView,

                    CompanyListView, CompanyDetailView, CompanyCreateView,
                    CompanyUpdateView, CompanyDeleteView,

                    ProjectListView, ProjectDetailView, ProjectCreateView,
                    ProjectUpdateView, ProjectDeleteView,

                    ProjectTimeListView, ProjectTimeCreateView, ProjectTimeDeleteView,
                    ProjectTimeDetailView, ProjectTimeUpdateView, ProjectTimeSpentDetails, ReportsView,UserViewSet
                    )
from rest_framework import routers
router = routers.DefaultRouter()
router.register(r'clients_new', UserViewSet)

urlpatterns = [
    url(r'^$', Dashboard.as_view(), name="dashboard")
]

urlpatterns += patterns(
    '',
    url(r'^clients/$',
        ClientListView.as_view(), name="client_list"),

    url(r'^clients/new/$',
        ClientCreateView.as_view(), name="client_new"),

    url(r'^clients/(?P<pk>\d+)/edit/$',
        ClientUpdateView.as_view(), name="client_edit"),

    url(r'^clients/(?P<pk>\d+)/$',
        ClientDetailView.as_view(), name="client_detail"),

    url(r'^clients/(?P<pk>\d+)/delete/$',
        ClientDeleteView.as_view(), name="client_delete"),

)

urlpatterns += patterns(
    '',
    url(r'^companys/$',
        CompanyListView.as_view(), name="company_list"),

    url(r'^company/new/$',
        CompanyCreateView.as_view(), name="company_new"),

    url(r'^companies/(?P<pk>\d+)/edit/$',
        CompanyUpdateView.as_view(), name="company_edit"),

    url(r'^companies/(?P<pk>\d+)/$',
        CompanyDetailView.as_view(), name="company_detail"),

    url(r'^companies/(?P<pk>\d+)/delete/$',
        CompanyDeleteView.as_view(), name="company_delete"),

)


urlpatterns += patterns(
    '',

    url(r'^projects/$',
        ProjectListView.as_view(), name="project_list"),

    url(r'^projects/new/$',
        ProjectCreateView.as_view(), name="project_new"),

    url(r'^projects/(?P<pk>\d+)/edit/$',
        ProjectUpdateView.as_view(), name="project_edit"),

    url(r'^projects/(?P<pk>\d+)/$',
        ProjectDetailView.as_view(), name="project_detail"),

    url(r'^projects/(?P<pk>\d+)/delete/$',
        ProjectDeleteView.as_view(), name="project_delete"),
)

urlpatterns += patterns(
    '',

    url(r'^project_time_spent/$',
        ProjectTimeListView.as_view(),
        name="project_time_spent_list"),

    url(r'^project_time_spent/new/$',
        ProjectTimeCreateView.as_view(),
        name="project_time_spent_new"),

    url(r'^project_time_spent/(?P<pk>\d+)/edit/$',
        ProjectTimeUpdateView.as_view(),
        name="project_time_spent_edit"),

    url(r'^project_time_spent/(?P<pk>\d+)/$',
        ProjectTimeDetailView.as_view(),
        name="project_time_spent_detail"),

    url(r'^project_time_spent/(?P<pk>\d+)/delete/$',
        ProjectTimeDeleteView.as_view(),
        name="project_time_spent_delete"),
)

urlpatterns += patterns(
    '',

    url(r'^reports/$',
        ReportsView.as_view(),
        name="reports"),
)

urlpatterns += patterns(
    '',
    url(r'^', include(router.urls)),
    url(r'^report_builder/', include('report_builder.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)