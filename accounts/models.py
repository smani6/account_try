from django.db import models
from decimal import Decimal
from django.core.urlresolvers import reverse

# Create your models here.

class Client(models.Model):
    """
        Client Model - Contains name, email and company info
    """

    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=200)
    company = models.ForeignKey("accounts.Company")
    created_datetime = models.DateTimeField(auto_now_add=True)
    modified_datetime = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("client_detail",
                       args=[self.pk])

class Company(models.Model):
    """
        Company Model - Contains name, description
    """

    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500,null=True,blank=True)
    created_datetime = models.DateTimeField(auto_now_add=True)
    modified_datetime = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("company_detail",
                       args=[self.pk])

class Project(models.Model):
    """
        Project Model - Contains name, and  project mapped to clients
    """

    client = models.ForeignKey("accounts.Client")
    name = models.CharField(max_length=200)
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    cost_per_hour = models.DecimalField(max_digits=5,decimal_places=2,default=Decimal(0))
    created_datetime = models.DateTimeField(auto_now_add=True)
    modified_datetime = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super(Project, self).save(*args, **kwargs)
        ProjectDetails.objects.get_or_create(
            project=self)

    def get_total_hours(self):
        project_statistics = ProjectTimeSpentDetails.objects.filter(
            project=self)
        total_hours = 0
        for each_project in project_statistics:
            hours = each_project.get_total_hours()
            total_hours += hours
        return total_hours

    def get_total_cost(self):
        total_cost = self.get_total_hours() * self.cost_per_hour
        return total_cost

    def get_absolute_url(self):
        return reverse("project_detail",
                       args=[self.pk])

class ProjectTimeSpentDetails(models.Model):
    """
    ProjectTimeSpentDetails - mapping to project with start time and end time and hours spent
    """

    project = models.ForeignKey(
        "accounts.Project")

    started_at = models.DateTimeField(null=True)

    ended_at = models.DateTimeField(null=True)

    hours_spent = models.DecimalField( max_digits=5,decimal_places=2,default=Decimal(0))

    def save(self, *args, **kwargs):
        date_delta = self.ended_at - self.started_at
        self.hours_spent = Decimal(date_delta.total_seconds() / 3600)
        super(ProjectTimeSpentDetails, self).save(*args, **kwargs)

    def get_total_hours(self):
        return self.hours_spent

    def get_absolute_url(self):
        return reverse("project_time_spent_detail",
                       args=[self.pk])

class ProjectDetails(models.Model):

    project = models.OneToOneField("accounts.Project")
    total_time_spent = models.DecimalField(max_digits=50,decimal_places=10,default=Decimal(0))
    total_cost = models.DecimalField( max_digits=50,decimal_places=10,default=Decimal(0))

    def get_total_hours(self):
        project_statistics, created = ProjectTimeSpentDetails.objects.get_or_create(
            project=self)
        return project_statistics.hours_spent

    def get_total_cost(self):
        return self.total_cost

